import 'package:flutter/material.dart';

///@date:  2021/3/1 13:48
///@author:  lixu
///@description: 加载数据失败view
class LoadErrorView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text(
          '加载失败\n点击重新加载',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 16,
            color: const Color(0xFFADADAD),
          ),
        ),
      ),
    );
  }
}
