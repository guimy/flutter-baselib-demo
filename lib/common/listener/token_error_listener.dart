import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:baselib_demo/common/event/token_error_event.dart';
import 'package:baselib_demo/module/login/view_model/logout_view_model.dart';

///@date:  2021/5/18 14:25
///@author:  lixu
///@description:token失效(登录信息失效)监听
class TokenErrorListener extends StatefulWidget {
  final Widget child;

  TokenErrorListener({required this.child});

  @override
  _TokenErrorListenerState createState() => _TokenErrorListenerState();
}

class _TokenErrorListenerState extends State<TokenErrorListener> {
  String _tag = "TokenErrorListener";

  late String _eventKey;

  @override
  void initState() {
    super.initState();
    LogUtils.i(_tag, 'initState 监听token失效(登录信息失效)事件');
    _eventKey = eventBus.on<TokenErrorEvent>((event) {
      LogUtils.e(_tag, '收到登录信息失效事件：${event.toString()}');
      context.read<LogoutViewModel>().onTokenErrorLogic(context, event);
    });
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }

  @override
  void dispose() {
    super.dispose();
    eventBus.off(_eventKey);
    LogUtils.i(_tag, 'dispose 取消监听token失效(登录信息失效)事件');
  }
}
