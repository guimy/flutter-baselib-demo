import 'dart:io';

///@date:  2021/5/19 10:56
///@author:  lixu
///@description: 文件管理工具
class FileUtils {
  ///文件文件夹
  ///[path] 文件路径
  static Future<Directory> createDir(String path) {
    Directory directory = Directory(path);
    return directory.create(recursive: true);
  }
}
