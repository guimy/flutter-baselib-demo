///@date:  2021/4/21 12:07
///@author:  lixu
///@description: 通用工具方法
class CommonUtils {
  CommonUtils._();

  ///获取毫秒时间戳
  static int currentTimeMillis() {
    return new DateTime.now().millisecondsSinceEpoch;
  }
}
