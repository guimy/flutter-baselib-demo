import 'dart:io';

import 'package:flutter/services.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:baselib_demo/common/util/env/ios_env_config.dart';

///@date:  2021/4/27 15:47
///@author:  lixu
///@description: 获取项目环境配置的插件
///本地调试运行或本地打包时：获取android工程下的config.gradle配置文件中的参数
///jenkins打包时：获取jenkins传递的配置参数
///可以根据需求在当前插件中获取更多的配置参数
class EnvConfigPlugin {
  EnvConfigPlugin._();

  static String _tag = 'SysEnvConfigPlugin';

  static const MethodChannel _channel = const MethodChannel('com.qw.flutter.plugins/env');

  ///获取服务器环境
  static const String _isReleaseServerMethod = "isReleaseServer";

  ///获取控制台是否打印日志状态
  static const String _isConsoleLogMethod = "isConsoleLog";

  ///代码是否混淆
  static const String _isProguardMethod = "isProguard";

  ///是否使用正式服
  static bool? _isReleaseServer;

  ///控制台是否打印日志
  static bool? _isConsoleLog;

  ///代码是否混淆
  static bool? _isProguard;

  ///是否使用正式服
  static Future<bool> isReleaseServer() async {
    if (_isReleaseServer == null) {
      if (!Platform.isAndroid) {
        _isReleaseServer = IosEnvConfig.isReleaseServer;
      } else {
        ///读取android/config.gradle配置文件中的isReleaseServer参数
        _isReleaseServer = await _channel.invokeMethod(_isReleaseServerMethod);
        LogUtils.e(_tag, 'isReleaseServer:$_isReleaseServer');
      }
    }

    return _isReleaseServer!;
  }

  ///控制台是否打印日志
  static Future<bool> isConsoleLog() async {
    if (_isConsoleLog == null) {
      if (!Platform.isAndroid) {
        _isConsoleLog = IosEnvConfig.isConsoleLog;
      } else {
        _isConsoleLog = await _channel.invokeMethod(_isConsoleLogMethod);
        LogUtils.e(_tag, 'isConsoleLog:$_isConsoleLog');
      }
    }
    return _isConsoleLog!;
  }

  ///代码是否混淆
  static Future<bool> isProguard() async {
    if (_isProguard == null) {
      if (!Platform.isAndroid) {
        _isProguard = false;
      } else {
        _isProguard = await _channel.invokeMethod(_isProguardMethod);
        LogUtils.e(_tag, 'isProguard:$_isProguard');
      }
    }
    return _isProguard!;
  }
}
