import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:baselib_demo/module/debug/view/debug_view.dart';
import 'package:baselib_demo/module/home/view/main_page.dart';
import 'package:baselib_demo/module/login/view/login_view.dart';
import 'package:baselib_demo/module/splash/view/splash_view.dart';

///@date:  2021/4/20 11:44
///@author:  lixu
///@description:定义路由handler
class RouterHandler {
  ///启动页
  static var rootHandler = Handler(handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
    return SplashView();
  });

  ///debug页
  static var debugHandler = Handler(handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
    return DebugView();
  });

  ///登录界面
  static Handler loginHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
      return LoginView();
    },
  );

  ///主界面
  static Handler homeHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
      return MainPage();
    },
  );
}
