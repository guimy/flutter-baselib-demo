import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:baselib_demo/common/router/routes.dart';

///@date:  2021/4/20 15:44
///@author:  lixu
///@description:页面导航管理
class NavigatorUtils {
  ///进入登录页,关闭启动页
  static void toLogin(context) {
    Routes.navigateTo(context, Routes.loginPage, clearStack: true, replace: true);
  }

  ///进入debug页面
  static void toDebugPage(context) {
    Routes.navigateTo(context, Routes.debugPage);
  }

  ///进入主页,关闭登录页
  static void toHome(context) {
    Routes.navigateTo(context, Routes.homePage, clearStack: true, replace: true);
  }

  ///关闭页面
  static void pop(context, {result}) {
    Navigator.of(context).pop(result);
  }

  ///退出应用
  static exitApp() {
    SystemNavigator.pop();
  }
}
