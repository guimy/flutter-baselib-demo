import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';

import 'app.dart';

///应用入口函数
void main() {
  FlutterBugly.postCatchException(
    () => runApp(App()),
    onCatchCallback: (String errMsg, errDetail) {
      ///上传日志到bugly时，回调该方法
      ToastUtils.showDebug('异常：$errDetail', isShowLong: true);
    },
  );
}
