import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:baselib_demo/common/util/ui_utils.dart';
import 'package:baselib_demo/module/debug/view_model/debug_viewmodel.dart';

///@date:  2021/4/21 15:06
///@author:  lixu
///@description: 系统信息View
class SysInfoView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<DebugViewModel>(builder: (context, viewModel, child) {
      return SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _getItem(context, '设备信息：', viewModel.deviceInfo),
            _getItem(context, '屏幕信息：', viewModel.screenInfo),
            _getItem(context, '包信息：', viewModel.appInfo),
            _getItem(context, '开发环境信息：', viewModel.devEnvInfo, isShowDivider: false),
            Visibility(
              visible: viewModel.isShowSwitchServerEnv,
              child: UIUtils.getButton('切换服务器环境', () {
                viewModel.onChangeSwitchServerEnv(context);
              }),
            ),
            Divider(height: 30, color: Colors.grey)
          ],
        ),
      );
    });
  }

  Widget _getItem(BuildContext context, String title, String desc, {bool isShowDivider = true}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(
            color: Colors.black,
            fontSize: 22,
            fontWeight: FontWeight.w600,
          ),
        ),
        Divider(height: 6, color: Colors.transparent),
        Text(
          desc,
          style: TextStyle(color: Colors.black87, fontSize: 14, height: 1.2),
        ),
        Visibility(
          visible: isShowDivider,
          child: Divider(
            height: 40,
            color: Colors.grey,
          ),
        ),
      ],
    );
  }
}
