import 'package:flutter_baselib/flutter_baselib.dart';

///@date:  2021/5/20 09:32
///@author:  lixu
///@description: 首页，我的
class MineViewModel extends BaseCommonViewModel {
  @override
  String getTag() {
    return 'MineViewModel';
  }
}
