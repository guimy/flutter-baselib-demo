import 'package:flutter_baselib/flutter_baselib.dart';

///@date:  2021/4/20 16:14
///@author:  lixu
///@description: 首页HomeView使用ViewModel
class HomeViewModel extends BaseCommonViewModel {
  @override
  String getTag() {
    return 'HomeViewModel';
  }
}
