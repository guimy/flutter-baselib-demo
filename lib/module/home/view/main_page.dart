import 'package:flutter/material.dart';
import 'package:flutter_baselib/flutter_baselib.dart';
import 'package:baselib_demo/common/listener/token_error_listener.dart';
import 'package:baselib_demo/common/util/assets_utils.dart';
import 'package:baselib_demo/module/home/view/mine_view_tab.dart';
import 'package:baselib_demo/module/home/view/welfare_view_tab.dart';
import 'package:baselib_demo/module/home/view_model/home_view_model.dart';
import 'package:baselib_demo/module/home/view_model/main_view_model.dart';
import 'package:baselib_demo/module/home/view_model/mine_view_model.dart';
import 'package:baselib_demo/module/home/view_model/welfare_view_model.dart';

import 'home_view_tab.dart';

///@date:  2021/4/20 16:11
///@author:  lixu
///@description: APP主页
class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) {
          return MainViewModel();
        }),
        ChangeNotifierProvider(create: (_) {
          return HomeViewModel();
        }),
        ChangeNotifierProvider(create: (_) {
          return WelfareViewModel();
        }),
        ChangeNotifierProvider(create: (_) {
          return MineViewModel();
        }),
      ],
      child: TokenErrorListener(
        child: _MainView(),
      ),
      builder: (context, child) {
        return WillPopScope(
          onWillPop: context.read<MainViewModel>().onWillPop(),
          child: child!,
        );
      },
    );
  }
}

class _MainView extends StatefulWidget {
  @override
  _MainViewState createState() => _MainViewState();
}

class _MainViewState extends State<_MainView> {
  String _tag = 'HomeView';

  ///当前显示的页面索引
  int _currentIndex = 0;
  late PageController _pageController;

  List _tabsText = ['首页', '福利', '我的'];
  List _tabsIcon = [
    'ic_home_main',
    'ic_home_welfare',
    'ic_home_mine',
  ];

  List<Widget> _pages = [
    ///首页
    HomeViewTab(),

    ///福利
    BaseView<WelfareViewModel>(
      child: WelfareViewTab(),
    ),

    ///我的
    MineViewTab(),
  ];

  @override
  void initState() {
    super.initState();
    _currentIndex = 0;
    _pageController = PageController(initialPage: _currentIndex);
  }

  @override
  Widget build(BuildContext context) {
    LogUtils.i(_tag, 'build:$_currentIndex');
    return Scaffold(
      body: PageView.builder(
        physics: NeverScrollableScrollPhysics(),
        itemCount: _pages.length,
        controller: _pageController,
        onPageChanged: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
        itemBuilder: (context, index) {
          return _pages[index];
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        elevation: 0,
        showUnselectedLabels: true,
        selectedItemColor: Colors.pinkAccent,
        unselectedItemColor: Colors.grey,
        currentIndex: _currentIndex,
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        items: [
          _getBottomNavigationBar(_tabsText[0], _tabsIcon[0]),
          _getBottomNavigationBar(_tabsText[1], _tabsIcon[1]),
          _getBottomNavigationBar(_tabsText[2], _tabsIcon[2]),
        ],
        onTap: (index) {
          if (_currentIndex != index) {
            _currentIndex = index;
            _pageController.jumpToPage(index);
            setState(() {});
          }
        },
      ),
    );
  }

  _getBottomNavigationBar(String label, String iconPath) {
    return BottomNavigationBarItem(
      label: label,
      activeIcon: _getBottomIcon('${iconPath}_selected'),
      icon: _getBottomIcon('${iconPath}_unselected'),
    );
  }

  _getBottomIcon(String icon) {
    return Container(
      alignment: Alignment.bottomCenter,
      child: Image.asset(
        AssetsUtils.loadAssetsImg(icon),
        width: 26,
        height: 26,
        fit: BoxFit.fitWidth,
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
